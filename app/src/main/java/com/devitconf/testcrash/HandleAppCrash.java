package com.devitconf.testcrash;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class HandleAppCrash implements Thread.UncaughtExceptionHandler {
    private final Context myContext;
//    Class<?> intentClass;
    private ArrayList<String> data;

    public HandleAppCrash(Context context, ArrayList<String> data) {
        this.myContext = context;
        this.data = data;
//        this.intentClass = intentClass;
    }

    public static void deploy(Context context, ArrayList<String> data) {
        Thread.setDefaultUncaughtExceptionHandler(new HandleAppCrash(context, data));
    }

    public void uncaughtException(Thread thread, Throwable exception) {
        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        System.err.println(stackTrace);
        saveList();
//        Intent intent = new Intent(this.myContext, this.intentClass);
//        intent.putExtra("stackTrace", stackTrace.toString());
//        this.myContext.startActivity(intent);
        System.exit(10);
    }

    private void saveList(){
        TinyDB tinydb = new TinyDB(this.myContext);

        tinydb.putListString("data", data);

    }

    public void setData(ArrayList<String> data) {
        this.data = data;
    }
}
