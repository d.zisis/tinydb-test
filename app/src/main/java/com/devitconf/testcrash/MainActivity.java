package com.devitconf.testcrash;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button _crashBtn;
    Button _saveDataBtn;
    Button _showDataBtn;

    EditText _dataSize;
    EditText _enterDataTxt;

    ArrayList<String> data;

    TinyDB tinydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        _crashBtn = (Button) findViewById(R.id.crashBtn);
        _saveDataBtn = (Button) findViewById(R.id.saveBtn);
        _showDataBtn = (Button) findViewById(R.id.showBtn);
        _dataSize = (EditText) findViewById(R.id.showDataSize);
        _enterDataTxt = (EditText) findViewById(R.id.enterData);

        tinydb  = new TinyDB(this);

        data = tinydb.getListString("data");
        tinydb.clear();

        // If there are no data on tinydb, initialize ArrayList
        if (data == null)
            data = new ArrayList<>();

        _saveDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(_enterDataTxt.getText().toString()))
                    data.add(_enterDataTxt.getText().toString());
            }
        });

        _crashBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer i=null;
                i.toString();
            }
        });

        _showDataBtn.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                _dataSize.setText("Data Size: "+data.size() + " Tinydb Data Size: "+tinydb.getListString("data").size());
            }
        });

        HandleAppCrash.deploy(this, data);
    }
}
